import { useForm } from "react-hook-form";
import { DateTime } from "luxon";
import { useMemo, useEffect } from "react";

//Component form for Company
export default function Form({ submitForm, company = {}, ...props }) {
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    defaultValues: useMemo(() => {
      if (company?.id) {
        return company;
      } else {
        return {};
      }
    }, [company]),
  });
  const now = DateTime.now().setLocale("ar");

  const submit = (data) => {
    submitForm(data);
  };

  useEffect(() => {
    if (company?.id) {
      reset(company);
    }
  }, [company]);
  return (
    <form className="row g-3" onSubmit={handleSubmit(submit)}>
      <div className="col-md-6">
        <label htmlFor="name" className="form-label">
          Nombre de la empresa
        </label>

        <input
          type="text"
          className="form-control"
          id="name"
          {...register("name", { required: true })}
          defaultValue={company?.name ? company?.name : ""}
        />
        {errors.name && (
          <div className="form-text text-danger">
            El Nombre de la empresa es requerido.
          </div>
        )}
      </div>
      <div className="col-md-6">
        <label htmlFor="address" className="form-label">
          Dirección
        </label>
        <input
          type="text"
          className="form-control"
          id="address"
          defaultValue={company?.address ? company?.address : ""}
          {...register("address", { required: true })}
        />
        {errors.address && (
          <div className="form-text text-danger">
            La Dirección es requerida.
          </div>
        )}
      </div>
      <div className="col-md-6">
        <label htmlFor="nit" className="form-label">
          NIT
        </label>
        <input
          type="text"
          className="form-control"
          id="nit"
          defaultValue={company?.nit ? company?.nit : ""}
          {...register("nit", { required: true })}
        />
        {errors.nit && (
          <div className="form-text text-danger">El NIT es requerido.</div>
        )}
      </div>
      <div className="col-md-6">
        <label htmlFor="phone" className="form-label">
          Teléfono
        </label>
        <input
          type="text"
          className="form-control"
          id="phone"
          defaultValue={company?.phone ? company?.phone : ""}
          {...register("phone", { required: true })}
        />
        {errors.phone && (
          <div className="form-text text-danger">El Teléfono es requerido.</div>
        )}
      </div>
      <div className="col-md-12">
        <small className="text-muted float-end">
          <i>Todos los campos son requeridos</i>
        </small>
      </div>
      <div className="col-12">
        <div className="d-grid">
          {company?.id ? (
            <button type="submit" className="btn btn-outline-warning ">
              Editar
            </button>
          ) : (
            <button type="submit" className="btn btn-outline-primary ">
              Guardar
            </button>
          )}
        </div>
      </div>
    </form>
  );
}

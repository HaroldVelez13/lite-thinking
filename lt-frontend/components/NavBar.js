import Link from "next/link";

//Component general For NavBar
export default function NavBar() {
  return (
    <nav className="navbar navbar-dark bg-dark">
      <div className="container-fluid">
        <Link href="/">
          <a className="navbar-brand text-warning" href="#">
            LITE THINKING
          </a>
        </Link>
        <h6 className="d-flex text-warning">Harold Velez</h6>
      </div>
    </nav>
  );
}

import {DateTime} from "luxon";
import { useRouter } from 'next/router'


//Component Table for Company
export default function Table({companies}) {
  const router = useRouter()

  const handleClick = (id)=>{
    router.push('/detalle/'+id)
  }
  return (

  <table className="table table-hover  mb-0">
    <thead>
      <tr className="table-warning">
        <th scope="col">Nombre de la empresa</th>
        <th scope="col">Dirección</th>
        <th scope="col">NIT</th>
        <th scope="col">Teléfono</th>
        <th scope="col">Fecha de creación</th>
      </tr>
    </thead>
    <tbody>
    {companies.map(company=>(
      <tr key={company.id} onClick={()=>handleClick(company.id)}>
        <td>{company.name}</td>
        <td>{company.address}</td>
        <td>{company.nit}</td>
        <td>{company.phone}</td>
        <td>{DateTime.fromISO(company.created_at).toFormat('ff')}</td>
      </tr>
    ))}
    </tbody>
  </table>
  )
}

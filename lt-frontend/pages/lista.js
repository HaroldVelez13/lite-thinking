import NavBar from "../components/NavBar";
import Table from "../components/Table";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import axios from "axios";

//Page List Company
export default function Home() {
  const router = useRouter();
  const [companies, setCompanies] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost:8000/api/company/")
      .then(function (response) {
        const data = response.data;
        setCompanies(data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);
  const handleClick = (e) => {
    e.preventDefault();
    router.push("/");
  };
  return (
    <>
      <NavBar />
      <div className="p-5 ">
        <div className="w-full card shadow-sm">
          <div className="card-header bg-dark text-warning">
            Lista de empresas
            <button
              className="float-end btn btn-light btn-sm"
              type="button"
              onClick={handleClick}
              title="Crear"
            >
              <i className="bi bi-plus-circle" role="img" aria-label="Add"></i>
            </button>
          </div>
          <div className="flex">
            <Table companies={companies} />
          </div>
        </div>
      </div>
    </>
  );
}

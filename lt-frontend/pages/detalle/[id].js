import NavBar from "../../components/NavBar";
import Form from "../../components/Form";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import axios from "axios";
import { DateTime } from "luxon";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

//Page Detail
export default function Detalle() {
  const router = useRouter();
  const [company, setCompany] = useState([]);
  const [id, setId] = useState([]);
  const [del, setDelete] = useState(false);

  useEffect(() => {
    const _id = router.query.id;
    setId(_id);
    axios
      .get("http://localhost:8000/api/company/" + _id + "/")
      .then(function (response) {
        const data = response.data ?? {};
        setCompany(data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);
  const handleClick = (e) => {
    e.preventDefault();
    router.push("/lista");
  };
  const handleClickEdit = (e) => {
    e.preventDefault();
    router.push("/editar/" + id);
  };

  const notify = async () => {
    await toast("¡La empresa #00" + id + " fue Eliminada con exito!");
    setTimeout(() => router.push("/lista"), 5300);
  };

  const handleClickDelete = (e) => {
    e.preventDefault();
    const headers = { "Content-Type": "application/json" };
    axios
      .delete("http://localhost:8000/api/company/" + id + "/", {
        headers: headers,
      })
      .then(function (response) {
        const data = response.data;
        notify();
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <>
      <NavBar />
      <div className="p-5 ">
        <div className="w-full card shadow-sm">
          <div className="card-header bg-dark text-warning">
            Detalle de la empresa #000{company?.id}
            {del && (
              <span className="bg-success">
                <button
                  className="float-end btn btn-danger btn-sm mx-1"
                  type="button"
                  onClick={handleClickDelete}
                  title="Eliminar"
                >
                  <i>Eliminar</i>
                </button>
                <button
                  className="float-end btn btn-light btn-sm mx-1"
                  type="button"
                  onClick={() => setDelete(false)}
                  title="Cancelar"
                >
                  <i>Cancelar</i>
                </button>
              </span>
            )}
            <button
              className="float-end btn btn-danger btn-sm mx-1"
              type="button"
              onClick={() => setDelete(true)}
              title="Eliminar"
            >
              <i className="bi bi-trash" role="img" aria-label="Eliminar"></i>
            </button>
            <button
              className="float-end btn btn-warning btn-sm mx-1"
              type="button"
              onClick={handleClickEdit}
              title="Editar"
            >
              <i
                className="bi bi-pencil-square"
                role="img"
                aria-label="Edit"
              ></i>
            </button>
            <button
              className="float-end btn btn-light btn-sm mx-1"
              type="button"
              onClick={handleClick}
              title="Volver a la lista"
            >
              <i className="bi bi-list" role="img" aria-label="List"></i>
            </button>
          </div>
          <div className="flex p-3">
            <div className=" mb-3">
              <div className="card-body">
                <h5 className="card-title mb-3">{company?.name}</h5>
                <p className="card-text mb-2">
                  Dirección:{" "}
                  <small className="text-muted">{company?.address}</small>
                </p>
                <p className="card-text mb-2">
                  NIT: <small className="text-muted">{company?.nit}</small>
                </p>
                <p className="card-text mb-2">
                  Teléfono:{" "}
                  <small className="text-muted">{company?.phone}</small>
                </p>
                <p className="card-text mt-3 mb-0">
                  <small className="text-muted">
                    Creada:{" "}
                    {DateTime.fromISO(company?.created_at).toFormat("ff")}
                  </small>
                  <br />
                  <small className="text-muted">
                    Editada:{" "}
                    {DateTime.fromISO(company?.update_at).toFormat("ff")}
                  </small>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ToastContainer />
    </>
  );
}

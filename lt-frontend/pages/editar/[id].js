import NavBar from "../../components/NavBar";
import Form from "../../components/Form";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import axios from "axios";
import { DateTime } from "luxon";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

//Page Edit
export default function Detalle() {
  const router = useRouter();
  const [company, setCompany] = useState([]);
  const [id, setId] = useState(0);

  useEffect(() => {
    const _id = router.query.id;
    setId(_id);
    axios
      .get("http://localhost:8000/api/company/" + _id + "/")
      .then(function (response) {
        const data = response.data ?? {};
        setCompany(data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);
  const handleClick = (e) => {
    e.preventDefault();
    router.push("/lista");
  };
  const notify = async () => {
    await toast("¡La Empresa #00" + id + " fue Editada con exito!");
    setTimeout(() => router.push("/lista"), 5300);
  };

  const handleSubmit = (data) => {
    console.log("en crear", data);
    const headers = { "Content-Type": "application/json" };
    axios
      .put("http://localhost:8000/api/company/" + id + "/", data, {
        headers: headers,
      })
      .then(function (response) {
        const data = response.data;
        notify();
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <>
      <NavBar />
      <div className="row justify-content-md-center">
        <div className="col-md-10 col-lg-8">
          <div className="p-5 ">
            <div className="w-full card shadow-sm">
              <div className="card-header bg-dark text-warning">
                Formulario de edición para la empresa #000{id}
                <button
                  className="float-end btn btn-light btn-sm"
                  type="button"
                  onClick={handleClick}
                  title="Volver a la lista"
                >
                  <i className="bi bi-list" role="img" aria-label="List"></i>
                </button>
              </div>
              <div className="flex p-3">
                <Form submitForm={handleSubmit} company={company} />
              </div>
            </div>
          </div>
        </div>
      </div>

      <ToastContainer />
    </>
  );
}

#!/bin/sh

sleep 0.1
echo "Init makemigrations"
python manage.py makemigrations company

sleep 0.1
echo "Init migrate"
python manage.py migrate company

exec "$@"

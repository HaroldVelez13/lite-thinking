from django.db import models


# Company models.
class Company(models.Model):
    name = models.CharField(max_length=150)
    address = models.CharField(max_length=200)
    nit = models.CharField(max_length=100)
    phone = models.CharField(max_length=50)    
    created_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

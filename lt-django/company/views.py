from rest_framework import viewsets
from .serializer import CompanySerializer
from .models import Company


#Company ViewSet
class CompanyViewSet(viewsets.ModelViewSet):
    queryset = Company.objects.all().order_by('pk')
    serializer_class = CompanySerializer


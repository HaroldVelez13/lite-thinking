from django.urls import include, path
from .views import CompanyViewSet
from rest_framework import routers

#Declared Url for comany
router = routers.DefaultRouter()
router.register(r'company', CompanyViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
]
